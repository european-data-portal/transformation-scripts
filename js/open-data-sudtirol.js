var jsontTemplate = { 

    "self": "{@schema($)}", 

    "schema": function(obj) {
        
        var output = {};
        
        output.name = input.name;
        output.title = input.title;
        output.notes = input.notes;

        if (input.author) {
            output.publisher = {};
            output.publisher.type = "http://xmlns.com/foaf/0.1/Agent";
            output.publisher.name = input.author;
            if (input.author_email) {
                output.publisher.email = "mailto:" + input.author_email;
            }            
        }

        if (input.holder) {
            output.contact_point = [{}];
            output.contact_point[0].type = "http://www.w3.org/2006/vcard/ns#Kind";
            output.contact_point[0].name = input.holder;
            if (input.contact) {
                output.contact_point[0].email = "mailto:" + input.contact;
            }            
        }
        
        if (input.temporal_end) {
            if (!output.temporal) {
                output.temporal = [{}];                
            }
            output.temporal[0].end_date = input.temporal_end;
        }

        if (input.temporal_start) {
            if (!output.temporal) {
                output.temporal = [{}];                
            }
            output.temporal[0].start_date = input.temporal_start;
        }

        if (input.license_id) output.license_id = input.license_id;
        if (input.license_title) output.license_title = input.license_title;
        if (input.url) output.url = input.url;

        if (input.creation_date) output.issued = input.creation_date;
        if (input.update_frequency) output.accrual_periodicity = {"label": input.update_frequency};
        
        if (input.tags) {
            output.tags = [];
            for (var tag in input.tags) {
                var t = {};
                t.name = input.tags[tag].name;
                output.tags.push(t);
            }
        }

        if (input.groups) {
            var groups = {};

            groups["boundaries"] = "regions-and-cities";
            groups["climatologymeteorologyatmosphere"] = "science-and-technology";
            groups["culture"] = "education-culture-and-sport";
            groups["demography"] = "population-and-society";
            groups["economy"] = "economy-and-finance";
            groups["environment"] = "environment";
            groups["farming"] = "agriculture-fisheries-forestry-and-food";
            groups["geoscientificinformation"] = "science-and-technology";
            groups["healthiness"] = "health";
            groups["knlowledge"] = "science-and-technology";
            groups["mobility"] = "transport";
            groups["political"] = "government-and-public-sector";
            groups["security"] = "justice-legal-system-and-public-safety";
            groups["sport"] = "education-culture-and-sport";
            groups["tourism"] = "education-culture-and-sport";
            groups["weather"] = "science-and-technology";
            groups["welfare"] = "population-and-society";

            output.groups = [];
            for (var group in input.groups) {
                if (input.groups[group].name in groups) {
                    output.groups.push({"name" : groups[input.groups[group].name]});
                }
            }
        }

        if (input.extras) {
            output.extras = [];
            for (var extra in input.extras) {
                var key = input.extras[extra].key;
                var value = input.extras[extra].value;
                switch (key) {
                    case "contact-name":
                        if (!contact_point) {
                            var contact_point = {};
                        }
                        contact_point.name = value;
                        break;
                    case "spatial":
                        output.extras.push({"key": key, "value": value});
                        break;
                    default:
                }
            }
        }
        
        if (contact_point && contact_point.name) {
            if (!output.contact_point) {
                output.contact_point = [];
            }
            contact_point.type = "http://www.w3.org/2006/vcard/ns#Kind";
            output.contact_point.push(contact_point);
        }
        
        if (input.resources) {
            output.resources = [];
            for (var res in input.resources) {
                var resource = {};
                resource.name = input.resources[res].name;
                if (input.resources[res].description) resource.description = input.resources[res].description;
                if (input.resources[res].format) resource.format = input.resources[res].format;
                if (input.resources[res].url) resource.access_url = [input.resources[res].url];
                output.resources.push(resource);            
            }
        }

        return JSON.stringify(output);
    }
};
