var jsontTemplate = {

    "self": "{@schema($)}",

    "schema": function(obj) {

        var output = {};

        output.name = input.name;
        output.title = input.title;
        output.notes = input.notes;

        if (input.author) {
            output.publisher = {};
            output.publisher.type = "http://xmlns.com/foaf/0.1/Agent";
            output.publisher.name = input.author;
            if (input.author_email) {
                output.publisher.email = "mailto:" + input.author_email;
            }
        }

        if (input.maintainer) {
            output.contact_point = [{}];
            output.contact_point[0].type = "http://www.w3.org/2006/vcard/ns#Kind";
            output.contact_point[0].name = input.maintainer;
            if (input.maintainer_email) {
                output.contact_point[0].email = "mailto:" + input.maintainer_email;
            }
        }

        if (input.license_id) output.license_id = input.license_id;
        if (input.license_title) output.license_title = input.license_title;
        if (input.url) output.url = input.url;

        if (input.tags) {
            output.tags = [];
            for (var tag in input.tags) {
                var t = {};
                t.name = input.tags[tag].name;
                output.tags.push(t);
            }
        }

        if (input.groups) {
            var groups = {};
            // define all your category mappings here
            // e.g.:
            groups["AGRI"] = "agriculture-fisheries-forestry-and-food";
            groups["EDUC"] = "education-culture-and-sport";
            groups["ENVI"] = "environment";
            groups["ENER"] = "energy";
            groups["TRAN"] = "transport";
            groups["TECH"] = "science-and-technology";
            groups["ECON"] = "economy-and-finance";
            groups["SOCI"] = "population-and-society";
            groups["HEAL"] = "health";
            groups["GOVE"] = "government-and-public-sector";
            groups["REGI"] = "regions-and-cities";
            groups["JUST"] = "justice-legal-system-and-public-safety";
            groups["INTR"] = "international-issues";

            output.groups = [];
            for (var group in input.groups) {
                if (input.groups[group].title in groups) {
                    output.groups.push({"name" : groups[input.groups[group].title]});
                }
            }
        }

        if (input.extras) {
            for (var extra in input.extras) {
                var key = input.extras[extra].key;
                var value = input.extras[extra].value;
                switch (key) {
                    // define your mappings here
                }
            }
        }

        if (input.resources) {
            output.resources = [];
            for (var res in input.resources) {
                var resource = {};
                resource.name = input.resources[res].name;
                if (input.resources[res].description) resource.description = input.resources[res].description;
                if (input.resources[res].format) resource.format = input.resources[res].format;
                if (input.resources[res].url) resource.access_url = [input.resources[res].url];
                if (input.resources[res].mimetype) resource.mediatype = input.resources[res].mimetype;
                output.resources.push(resource);
            }
        }

        return JSON.stringify(output);
    }
};
