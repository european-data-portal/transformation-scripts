var jsontTemplate = {

    "self": "{@schema($)}",

    "schema": function(obj) {

        var output = {};

        //standard meta data
        if (input.dataset_id) output.name = input.dataset_id;

        if (input.metas.default) {
          if (input.metas.default.landingPage) output.url = input.metas.default.landingPage;
          if (input.metas.default.title) output.title = input.metas.default.title;
          if (input.metas.default.description) output.notes = input.metas.default.description;
          if (input.metas.default.language) output.language = [ { "label": input.metas.default.language } ];
          if (input.metas.default.modified) output.modified = input.metas.default.modified;
          if (input.metas.default.license) output.license_title = input.metas.default.license;

          if (input.metas.default.publisher) {
              output.publisher = {};
              output.publisher.type = "http://xmlns.com/foaf/0.1/Organization";
              output.publisher.name = input.metas.default.publisher;
          }
        }

        //tags
        if (input.metas.default.keyword) {
            output.tags = [];
            for (var tag in input.metas.default.keyword) {
                output.tags.push({"name": input.metas.default.keyword[tag]});
            }
        }

/*      extra fields

        if (input.fields) {
            for (var extra in input.fields) {
                var label = input.fields[extra].label;
                var name = input.fields[extra].name
                var description = input.fields[extra].description;
                switch (label) {
                  // define your mappings here
                }
            }
        }
*/

/*      Fix Download URL

        if (input.distribution) {
            output.resources = [];
            for (var res in input.distribution) {
                var resource = {};
                resource.access_url = [input.distribution[res].accessURL];
                if (input.distribution[res].downloadURL) resource.download_url = [input.distribution[res].downloadURL];
                if (input.distribution[res].title) resource.name = input.distribution[res].title;
                if (input.distribution[res].description) resource.description = input.distribution[res].description;
                if (input.distribution[res].format) resource.format = input.distribution[res].format;
                if (input.distribution[res].mediaType) resource.mimetype = input.distribution[res].mediaType;
                output.resources.push(resource);
            }
        }
*/

        return JSON.stringify(output);
    }
};
