var jsontTemplate = {

    "self": "{@schema($)}",

    "schema": function(obj) {

        var output = {};

        //standard meta data
        if (input.resource) {
          if (input.resource.id) output.identifier = input.resource.id;
          if (input.resource.name) output.name = input.resource.name;
          if (input.resource.updatedAt) output.modified = input.resource.updatedAt;
          if (input.resource.description) output.notes = input.resource.description;
        }

        if (input.metadata) output.url = input.metadata.domain;

        //categories and tags
        /*
        if (input.classification) {
          if (input.classification.tags) {
            output.tags = [];
            for (var tag in input.classification.tags) {
              output.tags.push({"name": input.classification.tags.[tag]});
            }
          }

          //TODO map categories
          if (input.classification.categories) {
            var groups = {};

            //Agriculture, Fisheries, Forestry, foods
            groups["Agriculture"] = "agriculture-fisheries-forestry-and-food";
            //Education, culture and sport
            groups["Education"] = "education-culture-and-sport";
            //Environment
            groups["Environment"] = "environment";
            //Energy
            //Transport
            groups["Transport"] = "transport";
            //Science and technology
            //Economy and finance
            groups["Business & Economy"] = "economy-and-finance";
            //Population and social conditions
            groups["Society"] = "population-and-society";
            //Health
            groups["Health"] = "health";
            //Government, Public sector
            groups["Government"] = "government-and-public-sector";
            groups["Government Spending"] = "government-and-public-sector";
            //Regions; Cities
            groups["Towns & Cities"] = "regions-and-cities";
            groups["Mapping"] = "regions-and-cities";
            //Justice,  legal system, public safety
            groups["crime and justice"] = "justice-legal-system-and-public-safety";
            //International issues
            groups["Defence"] = "international-issues";

            output.groups = [];
            for (var category in input.classification.categories) {
                output.groups.push({"name" : groups[input.classification.categories.[category]});
            }
          }
        }
        */

        return JSON.stringify(output);
    }
};
