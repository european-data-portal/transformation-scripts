var jsontTemplate = { 

	"self": "{@schema($)}", 

	"schema": function(obj){

        var output = {};
        
        output.name = input.slug;
        output.title = input.title;
        output.notes = input.description;

        if (input.organization) {
            output.publisher = {};
            output.publisher.type = "http://xmlns.com/foaf/0.1/Organization";
            output.publisher.name = input.organization.name;
        }

		if (input.owner) {
            output.contact_point = [{}];
            output.contact_point[0].type = "http://www.w3.org/2006/vcard/ns#Kind";
            output.contact_point[0].name = input.owner.first_name + " " + input.owner.last_name;
		}
		
		if (input.created_at) output.issued = input.created_at;
		if (input.modified) output.modified = input.last_modified;

		if (input.license) output.license_id = input.license;
		
		if (input.temporal_coverage) {
			output.temporal = [{}];
			if (input.temporal_coverage.start) output.temporal[0].start_date = input.temporal_coverage.start;
			if (input.temporal_coverage.end) output.temporal[0].end_date = input.temporal_coverage.end;			
		}
		
		if (input.frequency) {
			output.accrual_periodicity = {};
			switch (input.frequency) {
				case "annual":
					output.accrual_periodicity.resource = "http://publications.europa.eu/resource/authority/frequency/ANNUAL";
					break;
				case "biannual":
					output.accrual_periodicity.resource = "http://publications.europa.eu/resource/authority/frequency/BIENNIAL";
					break;
				case "semimonthly":
					output.accrual_periodicity.resource = "http://publications.europa.eu/resource/authority/frequency/MONTHLY_2";
					break;
				case "semiannual":
					output.accrual_periodicity.resource = "http://publications.europa.eu/resource/authority/frequency/YEARLY_2";
					break;
				case "threeTimesAYear":
					output.accrual_periodicity.resource = "http://publications.europa.eu/resource/authority/frequency/YEARLY_3";
					break;
				case "threeTimesAMonth":
					output.accrual_periodicity.resource = "http://publications.europa.eu/resource/authority/frequency/MONTHLY_3";
					break;
				case "semiweekly":
					output.accrual_periodicity.resource = "http://publications.europa.eu/resource/authority/frequency/WEEKLY_2";
					break;
				case "threeTimesAWeek":
					output.accrual_periodicity.resource = "http://publications.europa.eu/resource/authority/frequency/WEEKLY_3";
					break;
				case "realtime":
					output.accrual_periodicity.resource = "http://publications.europa.eu/resource/authority/frequency/UPDATE_CONT";
					break;
				case "punctual":
					output.accrual_periodicity.resource = "http://publications.europa.eu/resource/authority/frequency/IRREG";
					break;
				case "fournightly":
					output.accrual_periodicity.resource = "http://publications.europa.eu/resource/authority/frequency/BIWEEKLY";
					break;
				default:
					output.accrual_periodicity.resource = "http://publications.europa.eu/resource/authority/frequency/" + input.frequency.toUpperCase();
					break;
			}
		}
		
        if (input.tags) {
            output.tags = [];
            for (var tag in input.tags) {
                output.tags.push({"name": input.tags[tag]});
            }
        }

        if (input.spatial && input.spatial.type) {
			if (!output.extras) output.extras = [];
			output.extras.push({"key": "spatial", "value": JSON.stringify(input.spatial)});
        }

		if (input.page) output.page = [input.page];
		
        if (input.resources) {
            output.resources = [];
            for (var res in input.resources) {
                var resource = {};
                resource.name = input.resources[res].title;
                if (input.resources[res].description) resource.description = input.resources[res].description;
                if (input.resources[res].format) resource.format = input.resources[res].format;
                if (input.resources[res].url) resource.access_url = [input.resources[res].url];

                if (input.resources[res].last_modified) resource.modified = input.resources[res].last_modified;
                if (input.resources[res].created_at) resource.issued = input.resources[res].created_at;
				if (input.resources[res].checksum) {
					resource.checksum = {};
					resource.checksum.algorithm = "http://spdx.org/rdf/terms#checksumAlgorithm_sha1";
					resource.checksum.checksum_value = input.resources[res].checksum.value;
				}
				if (input.resources[res].size) resource.size = input.resources[res].size;
                output.resources.push(resource);            
            }
        }
        
        return JSON.stringify(output);
	}
};
