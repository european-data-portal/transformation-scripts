var jsontTemplate = { 

    "self": "{@schema($)}", 

    "schema": function(obj) {
        
        var output = {};
        
        output.name = input.name;
        output.title = input.title;
        output.notes = input.notes;

        if (input.maintainer) {
            output.publisher = {};
            output.publisher.type = "http://xmlns.com/foaf/0.1/Agent";
            output.publisher.name = input.maintainer;
            if (input.maintainer_email) {
                output.publisher.email = "mailto:" + input.maintainer_email;
            }            
        }

        if (input.author) {
            output.contact_point = [{}];
            output.contact_point[0].type = "http://www.w3.org/2006/vcard/ns#Kind";
            output.contact_point[0].name = input.author;
            if (input.author_email) {
                output.contact_point[0].email = "mailto:" + input.author_email;
            }            
        }
        
        if (input.temporal_end) {
            if (!output.temporal) {
                output.temporal = [{}];                
            }
            output.temporal[0].end_date = input.temporal_end;
        }

        if (input.temporal_start) {
            if (!output.temporal) {
                output.temporal = [{}];                
            }
            output.temporal[0].start_date = input.temporal_start;
        }

        if (input.license_id) output.license_id = input.license_id;
        if (input.license_title) output.license_title = input.license_title;
        if (input.url) output.url = input.url;

        if (input.creation_date) output.issued = input.creation_date;
        if (input.frequency) output.accrual_periodicity = {"label": input.frequency};

        if (input.geographical_geonames_url) {
            var dcat_spatial = {"resource": input.geographical_geonames_url};
            if (input.geographical_name) {
                dcat_spatial.label = input.geographical_name;
            }
            output.dcat_spatial = [dcat_spatial];
        }

        if (input.tags) {
            output.tags = [];
            for (var tag in input.tags) {
                var t = {};
                t.name = input.tags[tag].name;
                output.tags.push(t);
            }
        }

        if (input.groups) {
            var groups = {};

            groups["agricoltura"] = "agriculture-fisheries-forestry-and-food";
            groups["ambiente"] = "environment";
            groups["amministrazione"] = "government-and-public-sector";
            groups["cat-meteo"] = "science-and-technology";
            groups["clima"] = "science-and-technology";
            groups["conoscenza"] = "science-and-technology";
            groups["demografia"] = "population-and-society";
            groups["economia"] = "economy-and-finance";
            groups["cultura"] = "education-culture-and-sport";
            groups["health"] = "health";
            groups["mobilita"] = "transport";
            groups["gestione-del-territorio"] = "government-and-public-sector";
            groups["sport"] = "education-culture-and-sport";
            groups["sicurezza"] = "justice-legal-system-and-public-safety";
            groups["politica"] = "government-and-public-sector";
            groups["wellbeing"] = "health";
            groups["turismo"] = "education-culture-and-sport";
            groups["welfare"] = "population-and-society";

            output.groups = [];
            for (var group in input.groups) {
                if (input.groups[group].name in groups) {
                    output.groups.push({"name": groups[input.groups[group].name]});
                }
            }
        }

        if (input.resources) {
            output.resources = [];
            for (var res in input.resources) {
                var resource = {};
                resource.name = input.resources[res].name;
                if (input.resources[res].description) resource.description = input.resources[res].description;
                if (input.resources[res].format) resource.format = input.resources[res].format;
                if (input.resources[res].url) resource.access_url = [input.resources[res].url];
                if (input.resources[res].mimetype) resource.mimetype = input.resources[res].mimetype;
                if (input.resources[res].size) resource.size = input.resources[res].size;
                output.resources.push(resource);
            }
        }

        return JSON.stringify(output);
    }
};
